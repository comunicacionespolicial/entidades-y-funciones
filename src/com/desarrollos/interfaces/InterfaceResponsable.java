/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desarrollos.interfaces;

import com.desarrollos.model.Responsable;

/**
 *
 * @author Roque
 */
public interface InterfaceResponsable extends FuncionesImplementar<Responsable>{
public Responsable buscarPorLogin(String usuario, String clave);    
}
