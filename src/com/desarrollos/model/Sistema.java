/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desarrollos.model;

import com.desarrollos.interfaces.InterfaceResponsable;
import com.desarrollos.interfaces.InterfaceSistema;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Roque
 */
public class Sistema implements Serializable, InterfaceSistema{
    private Integer Id;
    private String Titulo;
    private Date Alta;
    private Boolean Terminado;
    private Boolean Eliminado;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public Date getAlta() {
        return Alta;
    }

    public void setAlta(Date Alta) {
        this.Alta = Alta;
    }

    public Boolean getTerminado() {
        return Terminado;
    }

    public void setTerminado(Boolean Terminado) {
        this.Terminado = Terminado;
    }

    public Boolean getEliminado() {
        return Eliminado;
    }

    public void setEliminado(Boolean Eliminado) {
        this.Eliminado = Eliminado;
    }

    @Override
    public boolean crear(Sistema entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean modificar(Sistema entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Sistema entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Sistema buscarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sistema> buscarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
