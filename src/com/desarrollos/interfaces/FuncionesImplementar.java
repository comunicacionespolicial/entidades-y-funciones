/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desarrollos.interfaces;

import java.util.List;

/**
 *
 * @author Roque
 */
public interface FuncionesImplementar <T> {
    public boolean crear (T entity);
    public boolean modificar(T entity);
    public boolean eliminar(T entity);
    public T buscarPorId(Integer id);
    public List <T> buscarTodos ();
    
}
