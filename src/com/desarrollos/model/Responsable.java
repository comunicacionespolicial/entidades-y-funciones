/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.desarrollos.model;

import com.desarrollos.interfaces.InterfaceResponsable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roque
 */
public class Responsable implements Serializable, InterfaceResponsable {
    
    Integer Id;
    String Usuario;
    String Nombre;
    String Contacto;
    Integer Rol;
    Boolean Eliminado;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getContacto() {
        return Contacto;
    }

    public void setContacto(String Contacto) {
        this.Contacto = Contacto;
    }

    public Integer getRol() {
        return Rol;
    }

    public void setRol(Integer Rol) {
        this.Rol = Rol;
    }

    public Boolean getEliminado() {
        return Eliminado;
    }

    public void setEliminado(Boolean Eliminado) {
        this.Eliminado = Eliminado;
    }

    @Override
    public Responsable buscarPorLogin(String usuario, String clave) {
     Responsable Login= new Responsable ();
     Login.setNombre("Roque");
     Login.setId(1);
     Login.setRol(1);
     Login.setUsuario("RSM");
     Login.setContacto("45698");
     return Login;
     
   
        
        
       
    }

    @Override
    public boolean crear(Responsable entity) {
    return Boolean.TRUE;
    
    }

    @Override
    public boolean modificar(Responsable entity) {
    return Boolean.TRUE;      
    }

    @Override
    public boolean eliminar(Responsable entity) {
    return true;
    }

    @Override
    public Responsable buscarPorId(Integer id) {
        Responsable R = new Responsable();
         R.setNombre("Roque");
         R.setId(1);
         R.setRol(1);
         R.setUsuario("RSM");
         R.setContacto("45698");
         return R;
    }

    @Override
    public List<Responsable> buscarTodos() {
       List <Responsable> lst= new ArrayList<>();
        for (int i = 0; i < 10; i++) {
        Responsable R = new Responsable();
         R.setNombre("Roque "+i);
         R.setId(i+1);
         R.setRol(1);
         R.setUsuario("RSM "+i);
         R.setContacto("45698");
        lst.add(R);
        }
        return lst;
    }
    
    
    
    
    
}
